<?php

namespace App\Http\Controllers;

use App\Http\Requests\PostStoreRequest;
use App\Models\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class PostController extends Controller
{
    public function test($id=null){
    if($id === null){
    $posts = Post::orderBy('created_at','DESC')->get();

    // dd($posts);
    $loading = false;
       return view('test', compact(['loading', 'posts']));
    } else {
        $post = Post::findOrFail($id);
        return view('post', compact(['post']));
    }
    }

    public function add(){
        return view('add');
    }
    public function store(PostStoreRequest $request){
        $params = $request->validated();
        $file = Storage::put('public', $params['picture']);
        $params["picture"] = substr($file, 7);
         Post::create($params);
        // $post = new Post;

        // $post->title = request('title');
        // $post->description = request('desc');
        // $post->extrait = substr( request('desc') , 5);
        // $post->save();

        return redirect('/posts');
    }

    public function update($id,PostStoreRequest $request){
        $params = $request->validated();
        $post = Post::find($id);
        $post = $post->update($params);

        return redirect()->route("postDetails", $id);
    }
    public function delete($id){
        $post = Post::find($id);
        if(Storage::exists("public/$post->picture")) {
            Storage::delete("public/$post->picture");
        }
        $post->delete();

        return redirect('/posts');
    }
}
