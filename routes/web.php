<?php

use App\Http\Controllers\CommentController;
use App\Http\Controllers\PostController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// Route::get("/posts/{id}", function ($id) {
//     dd($id);
// });

// Route::get("/actu/{id?}", function ( $id = null) {
//     dd($id);
// });

Route::get("/posts/{id?}",[PostController::class, 'test'])->name("postDetails");
Route::get("/add",[PostController::class, 'add']);
Route::post("/add", [PostController::class, 'store']);
Route::put("/posts/{id}/update", [PostController::class, 'update'])->name("postUpdate");
Route::delete("/posts/{id}/delete", [PostController::class, 'delete'])->name("postDelete");
Route::post("/commentaires/{postId}", [CommentController::class, "store"])->name("commentAdd");
Route::delete("/commentaires/{id}", [CommentController::class, "delete"])->name("commentDelete");
